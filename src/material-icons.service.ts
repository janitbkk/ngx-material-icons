import { Injectable, Inject, PLATFORM_ID } from '@angular/core';
import { isPlatformBrowser } from '@angular/common';
import { DomSanitizer } from '@angular/platform-browser';
import { MatIconRegistry } from '@angular/material';

@Injectable()
export class MaterialIconsService {

  get isBrowser() {
    return isPlatformBrowser(this.platformId);
  }

  constructor(
    @Inject(PLATFORM_ID) private platformId,
    private matIconRegistry: MatIconRegistry,
    private sanitizer: DomSanitizer,
  ) {}

  registerSvgIcons(dirPath: string, icons: string[]) {
    if (this.isBrowser) {
      icons.forEach((iconName) => {
        const sanitizedIcon = this.sanitizer.bypassSecurityTrustResourceUrl(`${dirPath}/${iconName}.svg`);
        this.matIconRegistry.addSvgIcon(iconName, sanitizedIcon);
      });
    }
  }
}
