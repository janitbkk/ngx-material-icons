import { NgModule, ModuleWithProviders } from '@angular/core';
import { MatIconModule } from '@angular/material';
import { MaterialIconsService } from './material-icons.service';

export * from './material-icons.service';

@NgModule({
  imports: [MatIconModule],
  exports: [MatIconModule]
})
export class MaterialIconsModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: MaterialIconsModule,
      providers: [MaterialIconsService]
    };
  }
}
